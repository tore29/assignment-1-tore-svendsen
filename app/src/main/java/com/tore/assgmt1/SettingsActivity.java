package com.tore.assgmt1;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class SettingsActivity extends Activity {
    private String url;
    private EditText xmlurl_ch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Intent start = new Intent (this, MainActivity.class);


        SharedPreferences settings = getApplicationContext().getSharedPreferences("url", 0);
        url = settings.getString("url", url);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        xmlurl_ch = (EditText)findViewById(R.id.xmlurl_ch);
        xmlurl_ch.setText(url);

        final Button savbtn = (Button) findViewById(R.id.savbtn);
        savbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                url = xmlurl_ch.getText().toString();
                savePref(url);
                startActivity(start);
            }
        });
    }

    // Based on http://stackoverflow.com/questions/10962344/how-to-save-data-in-an-android-app by gobernador
    public String savePref(String url) {
        String saved = getResources().getString(R.string.saved);

        SharedPreferences settings = getApplicationContext().getSharedPreferences("url", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("url", url);

        // Apply the edits!
        editor.apply();

        xmlurl_ch.setText(url);
        return saved;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.XmlParser.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }
}
