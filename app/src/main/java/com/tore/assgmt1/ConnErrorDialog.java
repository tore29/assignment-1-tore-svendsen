package com.tore.assgmt1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Tore on 16.09.2014.
 * Based on http://developer.android.com/guide/topics/ui/dialogs.html
 */
public class ConnErrorDialog extends DialogFragment {
    Intent close;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.connerror)
                .setPositiveButton(R.string.cont, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Based on snippet from http://stackoverflow.com/questions/3226495/android-exit-application-code by Kartik
                        close.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        close.putExtra("EXIT", true);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}

