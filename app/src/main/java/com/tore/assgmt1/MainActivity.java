package com.tore.assgmt1;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends Activity {

    Intent close;
    MediaPlayer mp;
    SettingsActivity save;
    String url; //"http://tore29.com/assignment1/track2.xml";
    private TextView title, artist, copyright;
    private Uri file;
    private ImageView cover;
    private XmlParser obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // snippet from http://stackoverflow.com/questions/3226495/android-exit-application-code by Kartik
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

        // Snippet from: http://stackoverflow.com/questions/13136539/caused-by-android-os-networkonmainthreadexception by Dipak Keshariya
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        SharedPreferences settings = getApplicationContext().getSharedPreferences("url", 0);
        url = settings.getString("url", url);

        title = (TextView)findViewById(R.id.title_ch);
        artist = (TextView)findViewById(R.id.artist_ch);
        copyright = (TextView)findViewById(R.id.copyright_ch);
        cover = (ImageView)findViewById(R.id.cover_ch);

        if (!isNetworkAvailable()) {
            ConnErrorDialog connerr;
            connerr = new ConnErrorDialog();
            connerr.show(getFragmentManager(), "dialog");
        }

        if (isNetworkAvailable()) {
            if (url != null) {
                updateInfo();
            }
        }

        final Button plybtn = (Button) findViewById(R.id.plybtn);
        plybtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                plybtn.setText(music());
            }
        });
    }

    public void updateInfo(){
        obj = new XmlParser(url);
        obj.fetchXML();
        while(obj.parsingComplete)
        title.setText(obj.getTrackTitle());
        artist.setText(obj.getArtist());
        copyright.setText(obj.getCopyright());
        setImage(cover, obj.getCover());
        file = Uri.parse(obj.getFile());

    }

    // Based on: http://stackoverflow.com/questions/3118691/android-make-an-image-at-a-url-equal-to-imageviews-image by primpap and COD3BOY
    private void setImage(ImageView cover ,String url) {
        try {
            Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            cover.setImageBitmap(bitmap);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Snippet from on http://stackoverflow.com/questions/4238921/detect-whether-there-is-an-internet-connection-available-on-android by Alexandre Jasmin
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager;
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private String music() {

        String rs = getResources().getString(R.string.restart);
        String stop = getResources().getString(R.string.stop);

        if (mp != null ) {
            stopMusic();
            return rs;
        }
        startMusic();
        return stop;
    }

    private void startMusic() {

        mp = new MediaPlayer();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mp.setDataSource(getApplicationContext(), file);
            mp.prepare();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.start();
    }

    private void stopMusic() {
        mp.stop();
        mp.release();
        mp = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.XmlParser.

        // Code snippet based on http://developer.android.com/guide/topics/ui/menus.html
        Intent start = new Intent (this, SettingsActivity.class);

        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(start);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
