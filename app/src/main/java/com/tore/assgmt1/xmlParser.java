package com.tore.assgmt1;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * This class parses XML
 * Based on: http://www.tutorialspoint.com/android/android_xml_parsers.htm gathered 13-09-2013 (DD-MM-YYYY)
 *
 */
public class XmlParser {
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingComplete = true;
    private String title = "title";
    private String artist = "artist";
    private String cover = "cover";
    private String file = "filename";
    private String copyright = "copyright";
    private String urlString = null;
    public XmlParser(String url){
        this.urlString = url;
    }

    public String getTrackTitle(){
        return title;
    }
    public String getArtist(){
        return artist;
    }
    public String getCover(){
        return cover;
    }
    public String getFile(){
        return file;
    }
    public String getCopyright() {
        return copyright;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text=null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name=myParser.getName();
                switch (event){
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if(name.equals("title")){
                            title = text;
                        }
                        else if(name.equals("artist")){
                            artist = text;
                        }
                        else if(name.equals("cover")){
                            cover = text;
                        }
                        else if(name.equals("file")){
                            file = text;
                        }
                        else if(name.equals("copyright")) {
                            copyright = text;
                        }
                        break;
                }
                event = myParser.next();

            }
            parsingComplete = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void fetchXML(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection)
                    url.openConnection();
                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();

                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();

                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES
                            , false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();


    }


}
